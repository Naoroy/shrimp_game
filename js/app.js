/*jslint es6*/
const wrap = (function(){
    "use strict";

    function Shrimp(set) {

        this.el = document.createElement("img");
        this.el.id = set.id;
        this.el.style.width = `${set.size}px`;
        this.el.style.height = `${set.size}px`;
        this.el.classList.add(set.pos);
        this.el.src = "shrimp.png";
        this.el.onclick = (evt) => {
            let shrimp = evt.target;
            let active = document.querySelector(".activeShrimp");
            if (active && active !== shrimp) { active.classList.toggle("activeShrimp") }
            shrimp.classList.toggle("activeShrimp");
        };
    }

    function placeShrimp() {
        let boxes = document.querySelectorAll(".box");
        boxes.forEach(function (el, i) {
            el.id = "box" + (i + 1);
            
            if (i < 3 || i > 4) {
                let s = new Shrimp({
                    id: "shrimp" + (i + 1),
                    pos: i < 3 ?
                        "left":
                        "right",
                    size: 60
                });
                i < 3 ?
                    el.classList.add("leftParent")
                    : el.classList.add("rightParent");
                el.appendChild(s.el);
            }
        });
    }

    function moveShrimp(evt) {
        let shrimp = document.querySelector(".activeShrimp");
        // console.log(shrimp, evt.target);
        if (evt.target.className === "box" && shrimp) {
            let next = shrimp.parentElement.nextSibling.nextSibling;
            let nextNext;
            if (next) { nextNext = next.nextSibling.nextSibling; }
            let prev = shrimp.parentElement.previousSibling.previousSibling;
            let prevPrev;
            if (prev) { prevPrev = prev.previousSibling.previousSibling; }
            // console.log(next);
            // console.log(nextNext);
            // console.log(evt.target)
            if (shrimp.classList.contains("left") && evt.target === next) {
                console.log("go right");
                shrimp.parentElement.classList.toggle("leftParent");
                evt.target.appendChild(shrimp);
                shrimp.parentElement.classList.toggle("leftParent");
                shrimp.classList.toggle("activeShrimp");
            } else if (shrimp.classList.contains("left") && next.innerHTML && !nextNext.innerHTML && evt.target === nextNext) {
                console.log("hop right");
                shrimp.parentElement.classList.toggle("leftParent");
                evt.target.appendChild(shrimp);
                shrimp.parentElement.classList.toggle("leftParent");
                shrimp.classList.toggle("activeShrimp");
            } else if (shrimp.classList.contains("right") && evt.target === prev) {
                console.log("go left");
                shrimp.parentElement.classList.toggle("rightParent");
                evt.target.appendChild(shrimp);
                shrimp.parentElement.classList.toggle("rightParent");
                shrimp.classList.toggle("activeShrimp");
            } else if (shrimp.classList.contains("right") && prev.innerHTML && !prevPrev.innerHTML && evt.target === prevPrev) {
                console.log("hop left");
                shrimp.parentElement.classList.toggle("rightParent");
                evt.target.appendChild(shrimp);
                shrimp.parentElement.classList.toggle("rightParent");
                shrimp.classList.toggle("activeShrimp");
            } else {
                // evt.target.classList.remove("forbidden");
                // evt.target.classList.add("forbidden");
                console.log("forbidden move!");
                console.log(evt.target);
                evt.target.animate([
                    { // from
                        opacity: 1,
                        backgroundColor: "#F22"
                    },
                    { // to
                        opacity: 0,
                        backgroundColor: "#F22"
                        // backgroundColor: "#000"
                    }
                ], {
                    duration: 250,
                    easing: "ease"
                });
            }
        }
    }

    function resetAll() {
        boxes.forEach((el) => {
            el.innerHTML = "";
            el.classList.remove("leftParent");
            el.classList.remove("rightParent");
        });
        placeShrimp();
    }

    function init() {
        placeShrimp();
        let reset = document.querySelector(".reset");
        let boxes = document.querySelectorAll(".box")
        reset.onclick = resetAll;

        window.addEventListener("click", moveShrimp);
    };

    window.addEventListener("DOMContentLoaded", init);

}());
